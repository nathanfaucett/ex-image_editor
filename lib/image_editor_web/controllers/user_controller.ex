defmodule ImageEditorWeb.UserController do
  use ImageEditorWeb, :controller

  action_fallback(ImageEditorWeb.FallbackController)

  def current(conn, _params) do
    user = ImageEditorWeb.Auth.Guardian.Plug.current_resource(conn)

    conn
    |> render(ImageEditorWeb.UserView, "show.json", user: user)
  end
end
