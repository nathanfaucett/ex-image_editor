defmodule ImageEditorWeb.AuthController do
  use ImageEditorWeb, :controller

  alias ImageEditor.Repo
  alias ImageEditor.Users.User
  alias Ueberauth.Auth

  action_fallback(ImageEditorWeb.FallbackController)

  plug(Ueberauth)

  def sign_in_user(conn, %{"user" => user} = params) do
    case Repo.get_by(User, email: user.email) do
      nil ->
        sign_up_user(conn, params)

      user ->
        {:ok, token, claims} =
          ImageEditorWeb.Auth.Guardian.encode_and_sign(user, %{}, token_type: :access)

        conn = ImageEditorWeb.Auth.Guardian.Plug.sign_in(conn, user, claims)

        conn
        |> put_resp_header("authorization", "Bearer #{token}")
        |> send_token(params, token)
    end
  end

  def sign_up_user(conn, %{"user" => user} = params) do
    changeset =
      User.changeset(%User{}, %{
        email: user.email
      })

    case Repo.insert(changeset) do
      {:ok, user} ->
        {:ok, token, claims} =
          ImageEditorWeb.Auth.Guardian.encode_and_sign(user, %{}, token_type: :access)

        conn = ImageEditorWeb.Auth.Guardian.Plug.sign_in(conn, user, claims)

        conn
        |> put_resp_header("authorization", "Bearer #{token}")
        |> send_token(params, token)

      {:error, _changeset} ->
        conn
        |> put_status(422)
        |> render(ImageEditorWeb.ErrorView, "422.json")
    end
  end

  def sign_out_user(conn, _params) do
    conn
    |> put_status(204)
    |> json(%{})
    |> ImageEditorWeb.Auth.Guardian.Plug.sign_out()
  end

  def request(conn, _params) do
    conn
    |> put_status(404)
    |> render(ImageEditorWeb.ErrorView, "404.json")
  end

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_status(401)
    |> render(ImageEditorWeb.ErrorView, "401.json")
  end

  def callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
    info = auth_info(auth)

    case info do
      {:ok, user} ->
        sign_in_user(conn, Map.merge(params, %{"user" => user}))
    end
  end

  def identity_callback(%{assigns: %{ueberauth_auth: auth}} = conn, params) do
    case validate_password(auth) do
      :ok ->
        callback(conn, params)

      {:error, _reason} ->
        conn
        |> put_status(401)
        |> render(ImageEditorWeb.ErrorView, "401.json")
    end
  end

  defp auth_info(%Auth{} = auth) do
    {:ok, %{email: auth.info.email}}
  end

  defp send_token(conn, %{"token" => _}, token) do
    json(conn, %{data: %{token: token}})
  end

  defp send_token(conn, _params, token) do
    frontend_origin =
      Application.get_env(:image_editor, ImageEditorWeb.Endpoint)[:frontend_origin]

    redirect(conn, external: "#{frontend_origin}/oauth?token=#{token}")
  end

  defp validate_password(
         %Auth{info: %{email: email}, credentials: %{other: %{password: password}}} = _auth
       ) do
    case Repo.get_by(User, email: email) do
      nil ->
        changeset =
          User.changeset(%User{}, %{
            email: email,
            password: password
          })

        case Repo.insert(changeset) do
          {:ok, _user} ->
            :ok

          {:error, changeset} ->
            {:error, changeset}
        end

      user ->
        if Comeonin.Bcrypt.checkpw(password, user.encrypted_password) do
          :ok
        else
          {:error, :invalid_password}
        end
    end
  end
end
