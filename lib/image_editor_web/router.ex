defmodule ImageEditorWeb.Router do
  use ImageEditorWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :auth do
    plug(ImageEditorWeb.Auth.AccessPipeline)
  end

  scope "/", ImageEditorWeb do
    pipe_through(:api)

    scope "/auth" do
      get("/providers", AuthController, :providers)

      get("/:provider", AuthController, :request)

      post("/identity/callback", AuthController, :identity_callback)
      get("/:provider/callback", AuthController, :callback)
      post("/:provider/callback", AuthController, :callback)
    end

    scope "/user" do
      pipe_through(:auth)

      get("/", UserController, :current)
      delete("/sign_out", AuthController, :sign_out_user)
    end
  end

  scope "/", ImageEditorWeb do
    pipe_through(:auth)

    resources("/images", ImageController, except: [:new, :edit])
  end
end
