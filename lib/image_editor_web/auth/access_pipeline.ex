defmodule ImageEditorWeb.Auth.AccessPipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :image_editor,
    module: ImageEditorWeb.Auth.Guardian,
    error_handler: ImageEditorWeb.Auth.ErrorHandler

  plug(Guardian.Plug.VerifyHeader, claims: %{"typ" => "access"}, realm: "Bearer")
  plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource, ensure: true)
end
