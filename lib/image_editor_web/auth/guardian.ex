defmodule ImageEditorWeb.Auth.Guardian do
  use Guardian, otp_app: :image_editor

  alias ImageEditor.Repo
  alias ImageEditor.Users.User

  def subject_for_token(%User{} = user, _claims) do
    sub = to_string(user.id)
    {:ok, sub}
  end

  def subject_for_token(_, _) do
    {:error, :unknown_error}
  end

  def resource_from_claims(%{"sub" => id} = _claims) do
    case Repo.get(User, id) do
      nil -> {:error, :not_found}
      user -> {:ok, user}
    end
  end

  def resource_from_claims(_claims) do
    {:error, :unknown_error}
  end
end
