defmodule ImageEditorWeb.ImageView do
  use ImageEditorWeb, :view
  alias ImageEditorWeb.ImageView

  def render("index.json", %{images: images}) do
    %{data: render_many(images, ImageView, "image.json")}
  end

  def render("show.json", %{image: image}) do
    %{data: render_one(image, ImageView, "image.json")}
  end

  def render("image.json", %{image: image}) do
    url = ImageEditorWeb.Uploaders.Image.url({image.file, image})
    rel = Path.relative_to(url, "/priv/static")
    path = Path.join("/", rel)

    %{id: to_string(image.id), name: image.name, url: path, points: image.points}
  end
end
