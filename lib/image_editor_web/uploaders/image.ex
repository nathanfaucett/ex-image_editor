defmodule ImageEditorWeb.Uploaders.Image do
  use Arc.Definition
  use Arc.Ecto.Definition

  def __storage, do: Arc.Storage.Local

  def storage_dir(_version, {_file, scope}) do
    if Mix.env() == :test do
      "test/uploads/#{scope.uuid}"
    else
      "priv/static/uploads/#{scope.uuid}"
    end
  end

  def filename(version, {_file, _scope}) do
    version
  end
end
