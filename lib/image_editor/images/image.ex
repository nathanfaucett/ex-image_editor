defmodule ImageEditor.Images.Image do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset

  schema "images" do
    field(:name, :string, default: "")
    field(:uuid, Ecto.UUID)
    field(:file, ImageEditorWeb.Uploaders.Image.Type)
    field(:points, :string, default: "[]")

    timestamps()
  end

  @doc false
  def changeset(image, attrs) do
    image
    |> cast(attrs, [:name, :uuid, :points])
    |> check_uuid
    |> cast_attachments(attrs, [:file])
    |> validate_required([:uuid, :file])
  end

  defp check_uuid(changeset) do
    if get_field(changeset, :uuid) == nil do
      force_change(changeset, :uuid, Ecto.UUID.generate())
    else
      changeset
    end
  end
end
