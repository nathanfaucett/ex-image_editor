defmodule ImageEditor.Users.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:email, :string)
    field(:password, :string, virtual: true)
    field(:encrypted_password, :string)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    changeset =
      if Map.has_key?(attrs, :password) do
        user
        |> cast(attrs, [:email, :password])
        |> validate_required([:email, :password])
        |> validate_length(:password, min: 6)
        |> put_encrypted_pw
      else
        cast(user, attrs, [:email])
      end

    changeset
    |> unique_constraint(:email, message: "Email already taken")
  end

  defp put_encrypted_pw(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :encrypted_password, Comeonin.Bcrypt.hashpwsalt(password))

      _ ->
        changeset
    end
  end
end
