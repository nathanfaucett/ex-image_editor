defmodule ImageEditorWeb.UserControllerTest do
  use ImageEditorWeb.ConnCase

  alias ImageEditor.Repo
  alias ImageEditor.Users.User

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "should return 401 when password invalid", %{conn: conn} do
    conn =
      post(conn, "/auth/identity/callback", %{
        "email" => "invalid@domain.com",
        "password" => nil
      })

    assert json_response(conn, 401)
  end

  test "should sign in/up user with ueberauth identity and redirect to frontend", %{conn: conn} do
    conn =
      post(conn, "/auth/identity/callback", %{
        "email" => "valid@domain.com",
        "password" => "password"
      })

    assert html_response(conn, 302)
  end

  test "should sign in/up user with ueberauth identity and return access token", %{conn: conn} do
    conn =
      post(conn, "/auth/identity/callback?token", %{
        "email" => "valid@domain.com",
        "password" => "password"
      })

    assert %{"token" => _token} = json_response(conn, 200)["data"]
  end

  test "should fetch current user", %{conn: conn} do
    user =
      Repo.insert!(%User{
        email: "valid@domain.com"
      })

    {:ok, token, _} = ImageEditorWeb.Auth.Guardian.encode_and_sign(user, %{}, token_type: :access)

    conn =
      conn
      |> put_req_header("authorization", "Bearer #{token}")
      |> get("/user")

    assert json_response(conn, 200)["data"] == %{"email" => "valid@domain.com"}
  end

  test "should sign user out", %{conn: conn} do
    user =
      Repo.insert!(%User{
        email: "valid@domain.com"
      })

    {:ok, token, claims} =
      ImageEditorWeb.Auth.Guardian.encode_and_sign(user, %{}, token_type: :access)

    conn = ImageEditorWeb.Auth.Guardian.Plug.sign_in(conn, user, claims)

    conn =
      conn
      |> put_req_header("authorization", "Bearer #{token}")
      |> delete("/user/sign_out")

    assert json_response(conn, 204)
  end
end
