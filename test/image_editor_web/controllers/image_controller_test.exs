defmodule ImageEditorWeb.ImageControllerTest do
  use ImageEditorWeb.ConnCase

  alias ImageEditor.Repo
  alias ImageEditor.Users.User
  alias ImageEditor.Images
  alias ImageEditor.Images.Image

  setup_all do
    File.rm_rf(Path.expand(Path.join(__DIR__, "../../uploads")))
    :ok
  end

  setup %{conn: conn} do
    user =
      Repo.insert!(%User{
        email: "valid@domain.com"
      })

    {:ok, token, claims} =
      ImageEditorWeb.Auth.Guardian.encode_and_sign(user, %{}, token_type: :access)

    conn = ImageEditorWeb.Auth.Guardian.Plug.sign_in(conn, user, claims)

    {:ok,
     conn:
       conn
       |> put_req_header("accept", "application/json")
       |> put_req_header("authorization", "Bearer #{token}"),
     user: user}
  end

  describe "index" do
    test "lists all images", %{conn: conn} do
      conn = get(conn, image_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create image" do
    test "renders image when data is valid", %{conn: conn} do
      create_conn = post(conn, image_path(conn, :create), create_attrs())
      assert %{"id" => id} = json_response(create_conn, 201)["data"]

      show_conn = get(conn, image_path(conn, :show, id))
      assert %{"id" => _id, "url" => _url} = json_response(show_conn, 200)["data"]
    end

    test "renders image when partial data is valid", %{conn: conn} do
      create_conn = post(conn, image_path(conn, :create), create_partial_attrs())
      assert %{"id" => id} = json_response(create_conn, 201)["data"]

      show_conn = get(conn, image_path(conn, :show, id))
      assert %{"id" => _id, "url" => _url} = json_response(show_conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      create_conn = post(conn, image_path(conn, :create), invalid_attrs())
      assert json_response(create_conn, 422)["errors"] != %{}
    end
  end

  describe "update image" do
    setup [:create_image]

    test "renders image when data is valid", %{conn: conn, image: %Image{id: id} = image} do
      update_conn = put(conn, image_path(conn, :update, image), update_attrs())
      assert %{"id" => _id} = json_response(update_conn, 200)["data"]

      show_conn = get(conn, image_path(conn, :show, id))

      assert %{"id" => _id, "url" => _url} = json_response(show_conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, image: image} do
      update_conn = put(conn, image_path(conn, :update, image), invalid_attrs())
      assert json_response(update_conn, 422)["errors"] != %{}
    end
  end

  describe "delete image" do
    setup [:create_image]

    test "deletes chosen image", %{conn: conn, image: image} do
      delete_conn = delete(conn, image_path(conn, :delete, image))
      assert response(delete_conn, 204)

      assert_error_sent(404, fn ->
        get(conn, image_path(conn, :show, image))
      end)
    end
  end

  defp create_attrs(),
    do: %{file: build_upload("test/fixtures/test.png"), points: "[[[0, 0], [1, 1], [1, 0]]]"}

  defp create_partial_attrs(), do: %{file: build_upload("test/fixtures/test.png")}

  defp update_attrs(),
    do: %{
      file: build_upload("test/fixtures/update_test.png"),
      points: "[[[0, 0], [-1, -1], [-1, 0]]]"
    }

  defp invalid_attrs(), do: %{file: nil, points: nil}

  defp build_upload(path) do
    %{__struct__: Plug.Upload, path: path, filename: Path.basename(path)}
  end

  defp create_image(_) do
    {:ok, image} = Images.create_image(create_attrs())
    {:ok, image: image}
  end
end
