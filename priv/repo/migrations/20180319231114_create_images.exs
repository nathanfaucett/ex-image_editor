defmodule ImageEditor.Repo.Migrations.CreateImages do
  use Ecto.Migration

  def change do
    create table(:images) do
      add(:name, :string, default: "")
      add(:uuid, :uuid)
      add(:file, :string)
      add(:points, :text, default: "[]")

      timestamps()
    end
  end
end
