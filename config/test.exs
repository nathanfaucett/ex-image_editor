use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :image_editor, ImageEditorWeb.Endpoint,
  http: [port: 4001],
  server: false,
  frontend_origin: "http://test.imageeditor.com:8080/#"

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :image_editor, ImageEditor.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "image_editor_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :ueberauth, Ueberauth.Strategy.Github.OAuth,
  client_id: "c174ab569c2881bac9ab",
  client_secret: "13fb741ab38ef1176d5838b4ee6ae7bfcc37c04e"

config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  client_id: "768923158943-qt3ifc1fb4j057lu4lmm4lgra9jmc1cv.apps.googleusercontent.com",
  client_secret: "MR1JAeYG-7Mup4xAhVK5lURv"

config :cors_plug, origin: ["http://test.imageeditor.com:8080"]

config :bcrypt_elixir, log_rounds: 4
