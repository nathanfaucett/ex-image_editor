# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :image_editor, ecto_repos: [ImageEditor.Repo]

# Configures the endpoint
config :image_editor, ImageEditorWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "h/UEoD8MRKMu8ZYdu5VyE/Cgj0IwMAbNHWVmXaSQm1U285gXYEOMrrqnGwha3w6T",
  render_errors: [view: ImageEditorWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: ImageEditor.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

config :ueberauth, Ueberauth,
  base_path: "/auth",
  providers: [
    github:
      {Ueberauth.Strategy.Github,
       [
         scope: "user"
       ]},
    google:
      {Ueberauth.Strategy.Google,
       [
         scope: "email profile"
       ]},
    identity:
      {Ueberauth.Strategy.Identity,
       [
         uid_field: :email,
         callback_methods: ["POST"]
       ]}
  ]

config :image_editor, ImageEditorWeb.Auth.Guardian,
  issuer: "ImageEditorWeb",
  ttl: {30, :days},
  verify_issuer: false,
  secret_key:
    System.get_env("GUARDIAN_SECRET") ||
      "A+hsvP15kjXH0WljWjS1SaDGxf71B4uolDrjuz3GHTZ9zR0xg4jgmjp8Jac8FAu8"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
